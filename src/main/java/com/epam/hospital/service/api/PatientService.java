package com.epam.hospital.service.api;

import com.epam.hospital.model.Patient;
import com.epam.hospital.model.PatientAppointment;
import com.epam.hospital.model.PatientDiagnosis;

import java.util.List;

public interface PatientService {

    Patient getPatientById(int id);

    List<Patient> getAllPatients();

    List<Patient> getFirstPartOfPatients();

    List<Patient> getNextPartOfPatients();

    List<Patient> getPreviousPartOfPatients();

    List<Patient> updatePartOfPatients();

    Patient getPatientByPatientDiagnosis(PatientDiagnosis patientDiagnosis);
    Patient getPatientByPatientAppointment(PatientAppointment patientAppointment);

    boolean isPreviousPageAvailable();

    boolean isNextPageAvailable();

    boolean saveOrUpdatePatient(Patient patient);

    boolean deletePatient(Patient patient);
}
